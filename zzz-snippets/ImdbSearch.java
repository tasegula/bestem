package com.endava.fedes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ImdbSearch extends Thread{

	String query;
	LinkedList<String> result;
	
	public ImdbSearch(String query, LinkedList<String> result)
	{
		this.query = query;
		this.result = result;
	}
	
	public static String getInfoArcheus(String query)
	{
		HashMap<String, List<String>> headers;
		URL url;
		URLConnection con;
		int counter = 50;
		InputStream in;
		BufferedReader rd;
		String result;
		try
		{
			url = new URL("http://localhost:8082/movies/" + LegacySearch.modifyQueryString(query));
			con = url.openConnection();
		
			headers = new HashMap<>(con.getHeaderFields());
			
			result = headers.get(null).get(0);
			while(!result.equals("HTTP/1.1 200 OK") && result.equals("HTTP/1.1 202 Accepted"))
			{
				System.out.println(headers);
				con = url.openConnection();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					System.out.println("I couldn't wait... :(");
				}
			}
			
			if(result.equals("HTTP/1.1 200 OK"))
			{
				in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				rd = new BufferedReader(new InputStreamReader(in));
				String str = rd.readLine();
				
				return str;
			}
			
		    return null;
		}
		catch(IOException e){
			
		}
		return null;
	}
	
	private String getInfo()
	{
		try {
			return igor("http://localhost:8082/movies/" + LegacySearch.modifyQueryString(query), 25);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	private String igor(String link, int counter)
	{
		HashMap<String, List<String>> headers;
		URL url;
		URLConnection con;
		InputStream in;
		BufferedReader rd;
		String result;
		
		if(counter-- <= 0)
			return null;
		
		try
		{
			url = new URL(link);
			con = url.openConnection();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			headers = new HashMap<>(con.getHeaderFields());
			
			result = headers.get(null).get(0);
			
			if(result.equals("HTTP/1.1 202 Accepted"))
			{
				in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				rd = new BufferedReader(new InputStreamReader(in));
			    String str = rd.readLine();
			    
			    String[] stringArray = str.split("\"");
			    String linkNew = null;
			    for(int i = 0; i < stringArray.length; i++)
			    
			    	if(stringArray[i].equals("detail"))
			    	{
			    		linkNew = stringArray[i+2];
			    		break;
			    	}
			    
			    if(linkNew == null)
			    	return null;
			    return igor(linkNew, counter);
			}
			
			if(result.equals("HTTP/1.1 200 OK"))
			{
			
		    	in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				rd = new BufferedReader(new InputStreamReader(in));
			    String str = rd.readLine();
			    
			    return str;
			}
		}
		catch(IOException e){
		}

		return null;
	}
	
	public void run() {
        String auxRes = getInfo();
        result.add(auxRes);
    }
	
	public static void main(String[] args) throws UnsupportedEncodingException, InterruptedException {
		LinkedList<String> result = new LinkedList<String>();
		ImdbSearch x = new ImdbSearch("death", result);
		
		x.start();
		x.join();
		
		System.out.println(result);
	
	}
	
}
