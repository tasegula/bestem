package com.endava.fedes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class LegacySearch extends Thread{
	
	String query;
	LinkedList<String> result;
	
	public LegacySearch(String query, LinkedList<String> result)
	{
		this.query = query;
		this.result = result;
	}
	
	public static String modifyQueryString(String x) throws UnsupportedEncodingException
	{
		//return URLEncoder.encode(x, "ISO-8859-1");
		return x.replaceAll("\\ ", "%20");
	}
	
	private String getInfo()
	{
		HashMap<String, List<String>> headers;
		URL url;
		URLConnection con;
		try
		{
			url = new URL("http://127.0.0.1:8081/tracktv?query=" + modifyQueryString(query));
			con = url.openConnection();
		
		
			headers = new HashMap<>(con.getHeaderFields());
			
			if(headers.get(null).get(0).equals("HTTP/1.1 200 OK"))
			{
				InputStream in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				BufferedReader a = new BufferedReader(new InputStreamReader(in));
			    String str = a.readLine();
	
			    return str;
			}
		}
		catch(IOException e){
			
		}
		return null;
	}
	
	public void run() {
        String auxRes = getInfo();
        result.add(auxRes);
    }
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		LinkedList<String> result = new LinkedList<String>();
		LegacySearch x = new LegacySearch("death", result);
		
		x.start();
		x.join();
		
		System.out.println(result);
	}
}
