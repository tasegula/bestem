public class ResponseParser {
	
	public static ArrayList<JSONObject> parseLegacyResponse(String JSONString) {
		JSONObject searchEntries = new JSONObject(JSONString);
		JSONArray entriesArray = (JSONArray) searchEntries.get("content");
		ArrayList<JSONObject> entriesList = new ArrayList<>();
		for (int i = 0; i < searchEntries.getInt("noEntries"); i++) {
			JSONObject newJO = new JSONObject();
			JSONObject entry = entriesArray.getJSONObject(i);
			
			Iterator<String> it = entry.keys();
			while (it.hasNext()) {
				String key = it.next();
				newJO.put(key.toLowerCase(), entry.get(key));
			}
			
			entriesList.add(newJO);
		}
		return entriesList;
	}
		
		
	public static ArrayList<JSONObject> parseAsynchronousResponse(String JSONString) {
		JSONArray searchEntries = new JSONArray(JSONString);
		ArrayList<JSONObject> entriesList = new ArrayList<>();
		for (int i = 0; i < searchEntries.length(); i++) {
			JSONObject newJO = new JSONObject();
			JSONObject entry = searchEntries.getJSONObject(i);
			
			Iterator<String> it = entry.keys();
			while (it.hasNext()) {
				String key = it.next();
				newJO.put(key.toLowerCase(), entry.get(key));
			}
			
			entriesList.add(newJO);
		}
		return entriesList;
	}
}