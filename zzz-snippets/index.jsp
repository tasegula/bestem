
<html>
<head>
<title>Example1</title>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
<!-- CSS styles for standard search box -->
<style type="text/css">

#my_header{
	color: #eee;
	cursor: pointer;
	font: 26px Arial, Helvetica, sans-serif;
	font-weight: bold;
	height: 100px;
	position: absolute;
    top: 20%;
    left: 45%;
	margin-top: +10px;
  	margin-left: -30px;
}

#my_message{
	color: #eee;
	cursor: pointer;
	font: 18px Arial, Helvetica, sans-serif;
	height: 100px;
	position: absolute;
    top: 30%;
    left: 45%;
	margin-top: +10px;
  	margin-left: -120px;
	
}

body{

  /* font */
  font-family:calibri;

  /* fallback */
  background-color: #1e5799;
  background-repeat: repeat-x;

  /* Safari 4-5, Chrome 1-9 */
  background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#1a82f7), to(#2F2727));

  /* Safari 5.1, Chrome 10+ */
  background: -webkit-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

  /* Firefox 3.6+ */
  background: -moz-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

  /* IE 10 */
  background: -ms-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

  /* Opera 11.10+ */
  background: -o-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

}

#search-form {
	background: #e1e1ee; /* Fallback color for non-css3 browsers */
	width: 365px;
	position: fixed;
  	top: 50%;
  	left: 50%;
  	margin-top: -50px;
  	margin-left: -180px;

	/* Gradients */
	background: -webkit-gradient( linear,left top, left bottom, color-stop(0, rgb(73,93,144)), color-stop(1, rgb(33,43,144)));
	background: -moz-linear-gradient( center top, rgb(13,33,144)) 75%, rgb(63,83,144) 100%);

	/* Rounded Corners */
	border-radius: 17px;
	-webkit-border-radius: 17px;
	-moz-border-radius: 17px;

	/* Shadows */
	box-shadow: 1px 1px 2px rgba(0,0,0,.3), 0 0 2px rgba(0,0,0,.3);
	-webkit-box-shadow: 1px 1px 2px rgba(0,0,0,.3), 0 0 2px rgba(0,0,0,.3);
	-moz-box-shadow: 1px 1px 2px rgba(0,0,0,.3), 0 0 2px rgba(0,0,0,.3);
}

/*** TEXT BOX ***/
input[type="text"]{
	background: #fafafa; /* Fallback color for non-css3 browsers */

	/* Gradients */
	background: -webkit-gradient( linear, left bottom, left top, color-stop(0, rgb(250,250,250)), color-stop(1, rgb(230,230,230)));
	background: -moz-linear-gradient( center top, rgb(250,250,250) 0%, rgb(230,230,230) 100%);

	border: 0;
	border-bottom: 1px solid #fff;
	border-right: 1px solid rgba(255,255,255,.8);
	font-size: 16px;
	margin: 4px;
	padding: 5px;
	width: 250px;

	/* Rounded Corners */
	border-radius: 17px;
	-webkit-border-radius: 17px;
	-moz-border-radius: 17px;

	/* Shadows */
	box-shadow: -1px -1px 2px rgba(0,0,0,.3), 0 0 1px rgba(0,0,0,.2);
	-webkit-box-shadow: -1px -1px 2px rgba(0,0,0,.3), 0 0 1px rgba(0,0,0,.2);
	-moz-box-shadow: -1px -1px 2px rgba(0,0,0,.3), 0 0 1px rgba(0,0,0,.2);
}

/*** USER IS FOCUSED ON TEXT BOX ***/
input[type="text"]:focus{
	outline: none;
	background: #fff; /* Fallback color for non-css3 browsers */

	/* Gradients */
	background: -webkit-gradient( linear, left bottom, left top, color-stop(0, rgb(255,255,255)), color-stop(1, rgb(235,235,235)));
	background: -moz-linear-gradient( center top, rgb(255,255,255) 0%, rgb(235,235,235) 100%);
}

/*** SEARCH BUTTON ***/
input[type="submit"]{
	background: #44921f;/* Fallback color for non-css3 browsers */

	/* Gradients */
	background: -webkit-gradient( linear, left top, left bottom, color-stop(0, rgb(73,73,74)), color-stop(0.15, rgb(73,73,64)), color-stop(0.88, rgb(63,73,64)), color-stop(1, rgb(73,63,64)));
	background: -moz-linear-gradient( center top, rgb(73,73,74) 0%, rgb(73,73,64) 15%, rgb(63,73,64) 88%, rgb(73,63,64) 100%);

	border: 0;
	color: #eee;
	cursor: pointer;
	float: right;
	font: 16px Arial, Helvetica, sans-serif;
	font-weight: bold;
	height: 30px;
	margin: 4px 4px 0;
	text-shadow: 0 -1px 0 rgba(0,0,0,.3);
	width: 84px;
	outline: none;

	/* Rounded Corners */
	border-radius: 30px;
	-webkit-border-radius: 30px;
	-moz-border-radius: 30px;

	/* Shadows */
	box-shadow: -1px -1px 1px rgba(255,255,255,.5), 1px 1px 0 rgba(0,0,0,.4);
	-moz-box-shadow: -1px -1px 1px rgba(255,255,255,.5), 1px 1px 0 rgba(0,0,0,.2);
	-webkit-box-shadow: -1px -1px 1px rgba(255,255,255,.5), 1px 1px 0 rgba(0,0,0,.4);
}
/*** SEARCH BUTTON HOVER ***/
input[type="submit"]:hover {
	background: #4ea923; /* Fallback color for non-css3 browsers */

	/* Gradients */
	background: -webkit-gradient( linear, left top, left bottom, color-stop(0, rgb(89,222,27)), color-stop(0.15, rgb(83,179,38)), color-stop(0.8, rgb(66,143,27)), color-stop(1, rgb(54,120,22)));
	background: -moz-linear-gradient( center top, rgb(89,222,27) 0%, rgb(83,179,38) 15%, rgb(66,143,27) 80%, rgb(54,120,22) 100%);
}
input[type="submit"]:active {
	background: #4ea923; /* Fallback color for non-css3 browsers */

	/* Gradients */
	background: -webkit-gradient( linear, left bottom, left top, color-stop(0, rgb(89,222,27)), color-stop(0.15, rgb(83,179,38)), color-stop(0.8, rgb(66,143,27)), color-stop(1, rgb(54,120,22)));
	background: -moz-linear-gradient( center bottom, rgb(89,222,27) 0%, rgb(83,179,38) 15%, rgb(66,143,27) 80%, rgb(54,120,22) 100%);
}
</style>


</head>



<body>
<h2 id="my_header" align="center">Hello <%= System.getProperty("user.name") %></h2>

<h3 id="my_message"> Please enter something you want me to search:</h3>
<form id="search-form" action="search" method="post">
    
    <input type="text" name="searchTerms"/>
    <input type="submit" value="submit">
</form>




</body>
</html>
