<%@page import="java.util.HashMap"%>
<%@page import="java.util.LinkedList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%!
public String doiString(HashMap<String, String> data, int i) {
	String result = "";
	
	if(i % 2 == 0)
		result += "<tr id=\"even\"><td align=\"center\" width=\"20%\"><img   height=\"190\" src=\"" + data.get("poster") + "\"></td>";
	if(i % 2 == 1)
		result += "<tr id=\"odd\"><td align=\"center\" width=\"20%\"><img   height=\"190\" src=\"" + data.get("poster") + "\"></td>";
	result += "<td width=\"35%\"><table width=\"100%\" height=\"100%\" >";
	result += "<tr><td><h2>" + data.get("title") + "</h2>" + "<b>(" + data.get("country") + ", " +
				data.get("year")+ ")</b> </td></tr>";
	
	result += "<tr><td>" + data.get("genres") + "</td></tr>";
	
	result += "<tr><td><table>";
	result += "<tr><td><b>IMDB</b></td><td>" + "<a href=\"" + data.get("imdb_link") + "\">" + data.get("imdb_link") + "</a>" + "</td></tr>";
	
	// IMDB
	if (!data.get("imdbrating").equals("N/A")) { 
		result += "<tr><td></td><td>" + "<b>Rating: </b>" + data.get("imdbrating") + "/10 from " + data.get("imdbvotes") + " votes" + "</td></tr>";
	}
	else {
		result += "<tr><td></td><td>" + "<b>Rating: </b>Not available</td></tr>";
	}
	
	// TVRAGE
	if(data.get("tvrage_link") != null) {
		result += "<tr><td><b>TvRage</b></td><td>" + "<a href=\"" + data.get("tvrage_link") + "\">" + data.get("tvrage_link") + "</a>" + "</td></tr>";
	}
	else {
		result += "<tr><td><b>TvRage</b></td><td> Not available</td></tr>";
	}
	
	// TRACK TV
	if(data.get("url") != null) {
		result += "<tr><td><b>TRACK</b></td><td>" + "<a href=\"" + data.get("url") + "\">" + data.get("url") + "</a>" + "</td></tr>";
	}
	else {
		result += "<tr><td><b>TRACK</b></td><td> Not available </td></tr>";
	}
	
	result += "</table></td></tr>";
	
	result += "</table></td>";
	result += "<td width=\"45%\"><p>" + data.get("overview") + "</p>" + 
				"<br>" + "<a class=\"show\"  onclick=\"myFunction(" + i + ")\">See more</a>" + "</td>";
	result += "</tr>";
	
	if(i % 2 == 0)
		result += "<tr id=\"even\">";
	if(i % 2 == 1)
		result += "<tr id=\"odd\">";

	result += "<td>" + "<span class=\"hidden\" id=\"myP" + (i*3) + "\">" + "</span></td>";
	
	result += "<td>" + "<span class=\"hidden\" id=\"myP" + (i*3+1) + "\"><table>";
	result += "<tr><td valign=top><b>Actors </b></td><td>" 	+ data.get("people") + "</td></tr>";
	result += "<tr><td><b>Director </b></td><td>" 	+ data.get("director") + "</td></tr>";
	result += "<tr><td><b>Writers </b></td><td>" 	+ data.get("writer") + "</td></tr>";
	result += "</table></span></td>";
	
	result += "<td>" + "<span class=\"hidden\" id=\"myP" + (i*3+2) + "\"><table>";
	result += "<tr><td><b>Awards</b></td><td>" + data.get("awards") + "</td></tr>";
	result += "<tr><td><b>Watch on</b></td><td>" + data.get("network") + ", " + 
													data.get("air_day") + " at " + 
													data.get("air_time") + " " + 
													data.get("timezone") + " (" + 
													data.get("runtime") + "min)" +
													"</td></tr>";
	result += "<tr><td><b>Status</b></td><td>" + data.get("status") + "</td></tr>";
	result += "</table></span></td>";
	
	result += "</tr>";
	
	return result;
}

%>


<html>

<style type="text/css">

span.hidden {display:none;}


a.show {
	text-decoration: none;
	color: #36f;
}

a.show:hover {
	border-bottom: 1px dotted #36f;
	cursor: pointer;
}




body{

  /* font */
  font-family:calibri;

  /* fallback */
  background-color: #1e5799;
  background-repeat: repeat-x;

  /* Safari 4-5, Chrome 1-9 */
  background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#1a82f7), to(#2F2727));

  /* Safari 5.1, Chrome 10+ */
  background: -webkit-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

  /* Firefox 3.6+ */
  background: -moz-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

  /* IE 10 */
  background: -ms-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

  /* Opera 11.10+ */
  background: -o-linear-gradient(top, #1e5799 0%,#a5c9e5 100%);

}


#table_back{

  	background: -webkit-linear-gradient(top, #eeeeff 0%,#ffffff 100%);
  	margin-top: 100px;
  	margin-bottom: 50px;
  	
}

#even{
	background-color:#EEEEEE;
}
#odd{
	background-color:#FFFFFF;
}
</style>

<script>
function myFunction(int)
{
var x = int * 3;
var string = "myP" + x;
if(document.getElementById(string).style.display != "inline")
document.getElementById(string).style.display="inline";
else
document.getElementById(string).style.display="none";

x = int * 3 + 1;
string = "myP" + x;
if(document.getElementById(string).style.display != "inline")
document.getElementById(string).style.display="inline";
else
document.getElementById(string).style.display="none";

x = int * 3 + 2;
string = "myP" + x;
if(document.getElementById(string).style.display != "inline")
document.getElementById(string).style.display="inline";
else
document.getElementById(string).style.display="none";
}
</script>


<head>
    <title>HoByte FEDES Results</title>
</head>
<body>
		
          <p><h1 align="center"><a href="http://d24w6bsrhbeh9d.cloudfront.net/photo/900362_700b.jpg" color=#FFFFFF> HoByte Search </a></h1></p>
          <p><h3 align="center"><a href="http://localhost:8080/fedes/" color=#FFFFFF> <-- Back </a></h3></p>
          <table id="table_back" align="center" width="85%" height="80%" CELLSPACING="0">
          <%
			LinkedList<HashMap<String, String>> res = (LinkedList<HashMap<String, String>>)request.getAttribute("results");
			
			int i = 0;
			for(HashMap<String, String> e : res)
			{
				if (e != null) {
					i++;
					out.println("<tr>" + doiString(e, i) + "</tr>");
				}
			}
          %>
		</table>
</body>
</html>
