package com.endava.fedes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Main {
	public static void main(String[] args) throws IOException, InterruptedException
	{
		String result = "http://localhost:8083/tvrage/query/house?callback=http%3A%2F%2Flocalhost%3A8080%2Fcallback%3Fqueried_for%3Dhouse";

		URL url = new URL(result);
		URLConnection con = url.openConnection();

		System.out.println(con.getHeaderFields());
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;


	    BufferedReader a = new BufferedReader(new InputStreamReader(in));
	    String str = a.readLine();
	    System.out.println(str);
	}
}
