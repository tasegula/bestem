package com.endava.fedes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.*;

/*
 * Class that queries the callback server and stores the result in the result field;
 * */
public class TVRAGESearch extends Thread{
	/*
	 * The field in which, at the end of the interrogation the result will be stored;
	 * */
	private static LinkedList<JSONObject> result;
	String query;
	/*
	 * Because this server can be queried based on a single token query, launching multiple
	 * queries in case of a multi-token user search phrase may lead to a series of duplicated
	 * results. The showIds field is used to filter the result by adding only the shows that 
	 * weren't added before.
	 * */
	public static LinkedList<String> showIds = new LinkedList<String>();
	private static final Object lock = new Object();
	
	public TVRAGESearch(String query, LinkedList<JSONObject> result){
		this.result = result;
		this.query = query;
	}
	
	/*
	 * A static method that is called by the listener servlet witch receives the query response.
	 * This method filters the results and lets the object continue it's queries, if necessary.
	 * */
	public static void putResult(String res){
		synchronized(result){
			if(showIds.size() == 0){
				result.addAll(ResponseParser.parseAsynchronousResponse(res));
				for(JSONObject tvshow: result){
					showIds.add(tvshow.getString("showid"));
				}
			}else{
				for(JSONObject tvshow: ResponseParser.parseAsynchronousResponse(res)){
					if(!showIds.contains(tvshow.get("showid"))){
						result.add(tvshow);
					}
				}
			}
		}
		
		synchronized(lock){
			lock.notify();
		}
	}
	
	/*
	 * Method that launches queries until the HTTP 200 OK response is received, for each  token
	 * of the search phrase. Afterwards it locks in a wait call until the query response is ready.
	 * */
	private void igor(String query) throws IOException, InterruptedException{
		String address = "http://localhost:8083/tvrage/query/%s?callback=http%3A%2F%2Flocalhost%3A8080%2Ffedes%2Fcallback%3Fqueried_for%3D%s";
		String q = null;
		String[] tokens = query.split(" ");
		
		for(String token: tokens){
			q = address.replaceAll("%s", token);
			while(true){
				URL url = new URL(q);
				URLConnection con = url.openConnection();
				Map<String, List<String>> header = con.getHeaderFields();
				if(header.get(null).get(0).contains(" 200 ")){
					synchronized(lock){
						lock.wait();
					}
					break;
				}
			}
		}
			
	}
	
	/*
	 * Launches the interrogation process.
	 * */
	public void run(){
		try {
			igor(query);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}
