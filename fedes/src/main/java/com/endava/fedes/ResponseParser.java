package com.endava.fedes;

import java.util.LinkedList;
import java.util.Iterator;

import org.json.*;

public class ResponseParser {
	
	public static LinkedList<JSONObject> parseLegacyResponse(String jsonString) {
		JSONObject searchEntries = new JSONObject(jsonString);
		JSONArray entriesArray = (JSONArray) searchEntries.get("content");
		LinkedList<JSONObject> entriesList = new LinkedList<JSONObject>();
		
		for (int i = 0; i < searchEntries.getInt("noEntries"); i++) {
			JSONObject newJO = new JSONObject();
			JSONObject entry = entriesArray.getJSONObject(i);
			
			Iterator<String> it = entry.keys();
			while (it.hasNext()) {
				String key = it.next();
				newJO.put(key.toLowerCase(), entry.get(key));
			}
			
			entriesList.add(newJO);
		}
		return entriesList;
	}
		
		
	public static LinkedList<JSONObject> parseAsynchronousResponse(String jsonString) {
		JSONArray searchEntries = new JSONArray(jsonString);
		LinkedList<JSONObject> entriesList = new LinkedList<JSONObject>();
		
		for (int i = 0; i < searchEntries.length(); i++) {
			JSONObject newJO = new JSONObject();
			JSONObject entry = searchEntries.getJSONObject(i);
			
			Iterator<String> it = entry.keys();
			while (it.hasNext()) {
				String key = it.next();
				newJO.put(key.toLowerCase(), entry.get(key));
			}
			
			entriesList.add(newJO);
		}
		
		return entriesList;
	}
}