package com.endava.fedes;

import javax.servlet.ServletException;
import javax.servlet.descriptor.JspPropertyGroupDescriptor;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;


/**
 * Base servlet for Federated Search.
 *
 * @author <a href="mailto:roxana.paduraru@endava.com">Roxana PADURARU</a>
 * @since 3/24/14
 */
public class FedesServlet extends HttpServlet {
	String jsonIMDB;
	public static LinkedList<JSONObject> IMDBresult = new LinkedList<JSONObject>();
	public static LinkedList<JSONObject> LEGACYresult = new LinkedList<JSONObject>();
	public static LinkedList<JSONObject> TVRAGEresult = new LinkedList<JSONObject>();
	
	/*
	 * Lists of TV Shows information that is considered irrelevant. Stored for possible later
	 * use.
	 * */
	public static LinkedList<JSONObject> IMDBresultIrrelevant = new LinkedList<JSONObject>();
	public static LinkedList<JSONObject> LEGACYresultIrrelevant = new LinkedList<JSONObject>();
	
	/*
	 * List that contains the aggregated search results in a form that is easily comprehensible
	 * by the interface.
	 * */
	public static LinkedList<HashMap<String, String>> finalForm = new LinkedList<>();
	
	/*
	 * Method that aggregates the information about a single TV Show. 
	 * The starting point is the information procured from the legacy server because it contains
	 * fields(imdb_id and tvrage_id) which makes it possible to harvest the information from the
	 * appropriate entries from the Pooling and Callback servers.
	 * */
	public HashMap<String,String> getTvShowMap(JSONObject legacy){
		JSONObject tvrage = null;
		JSONObject imdb = null;
		String key;
		String imdbLink = "http://www.imdb.com/title/";
		HashMap<String, String> m = new HashMap<>();
		Iterator<String> it = legacy.keys();
		
		while(it.hasNext()){
			key = it.next();
			if(key.equals("people")){
				m.put(key, getActors(legacy.get(key)));
				continue;
			}
			if(key.equals("genres")){
				m.put(key, getGenres(legacy.get(key)));
				continue;
			}
			m.put(key, legacy.get(key).toString());
		}
		
		imdbLink += m.get("imdb_id");
		m.put("imdb_link", imdbLink);
		
		for(JSONObject show: TVRAGEresult){
			if(show.get("showid").equals(m.get("tvrage_id"))){
				tvrage = show;
				break;
			}
		}
		
		if(tvrage != null){
			if(tvrage.getString("showlink") != null)
				m.put("tvrage_link", tvrage.getString("showlink"));
			else
				m.put("tvrage_link", "N/A");
			
			if( tvrage.getString("status") != null)
				m.put("status", tvrage.getString("status"));
			else
				m.put("status", "N/A");
			
			if(tvrage.getString("runtime") != null)
				m.put("runtime", tvrage.getString("runtime"));
			else
				m.put("runtime", "N/A");
			
			if(tvrage.getString("timezone") != null)
				m.put("timezone", tvrage.getString("timezone"));
			else
				m.put("timezone", "N/A");
			
			if(tvrage.getString("startdate") != null)
				m.put("startdate", tvrage.getString("startdate"));
			else
				m.put("startdate", "N/A");
			
			if(tvrage.getString("seasons") != null)
				m.put("seasons", tvrage.getString("seasons"));
			else
				m.put("seasons", "N/A");
		}else{
			m.put("runtime", "N/A");
			m.put("timezone", "N/A");
			m.put("startdate", "N/A");
			m.put("seasons", "N/A");
		}
		
		for(JSONObject show: IMDBresult){
			//System.out.println(show.get("imdbid") + "   " + m.get("imdb_id") );
			if(show.get("imdbid").equals(m.get("imdb_id"))){
				imdb = show;
				break;
			}
		}
		
		if(imdb != null){
			if(imdb.getString("poster") != null)
				m.put("poster", imdb.getString("poster"));
			else
				m.put("poster", "N/A");
			
			if(imdb.getString("genre") != null)
				m.put("genres", imdb.getString("genre"));
			else
				m.put("genres", "N/A");
			
			if(imdb.getString("director") != null)
				m.put("director", imdb.getString("director"));
			else
				m.put("director", "N/A");
			
			if(imdb.getString("writer") != null)
				m.put("writer", imdb.getString("writer"));
			else
				m.put("writer", "N/A");
			
			if(imdb.getString("language") != null)
				m.put("language", imdb.getString("language"));
			else
				m.put("language", "N/A");
			
			if(imdb.getString("awards") != null)
				m.put("awards", imdb.getString("awards"));
			else
				m.put("awards", "N/A");
			
			if(imdb.getString("imdbrating") != null)
				m.put("imdbrating", imdb.getString("imdbrating"));
			else
				m.put("imdbrating", "N/A");
			
			if(imdb.getString("imdbvotes") != null)
				m.put("imdbvotes", imdb.getString("imdbvotes"));
			else
				m.put("imdbvotes", "N/A");
			
			if(imdb.getString("actors") != null)
				m.put("people", imdb.getString("actors"));
			
		}else{
			m.put("director", "N/A");
			m.put("writer", "N/A");
			m.put("language", "N/A");
			m.put("awards", "N/A");
			m.put("imdbrating", "N/A");
			m.put("imdbvotes", "N/A");
		}
		
		return  m;
	}
	
	/*
	 * Formats the actor field from the legacy server in a pretty format for the interface.
	 * */
	private String getActors(Object object) {
		JSONObject jo =(JSONObject)object;
		JSONArray js = jo.getJSONArray("actors");
		JSONObject actor;
		String actors = "";
		for(int i = 0; i < js.length(); i++){
			actor = js.getJSONObject(i);
			if (actor.get("name") instanceof String)
				actors += actor.get("name") + ", ";
		}
		if (js.length() == 0) return "Not Available";
		
		return actors.substring(0, actors.length() - 2);
	}
	
	/*
	 * Formats the genres field from the legacy server in a pretty format for the interface.
	 * */
	private String getGenres(Object object) {
		JSONArray js = (JSONArray)object;
		String genres = "";
		
		for (int i = 0; i < js.length(); i++) {
			genres += js.getString(i) + ", ";
		}
		
		if (genres.length() == 0) return "Not Available";
		return genres.substring(0, genres.length() - 2);
	}

	public void getFinalForm(){
		finalForm.clear();
		for(JSONObject show: LEGACYresult){
			finalForm.add(getTvShowMap(show));
		}
	}
	
	/*
	 * Splits the results from the Legacy and Pooling servers in relevant and irrelevant informtion
	 * based on: title, year, actors matches.
	 * */
	public void sortbyRelevance(String query){
		String[] tokens = query.split(" ");
		int flag = 0;
		Iterator<JSONObject> it;
		it = IMDBresult.iterator();
		JSONObject tvshow;
		
		for(int i = 0; i < IMDBresult.size(); i++){
			flag = 0;
			tvshow = IMDBresult.get(i);
			for(String token: tokens){
				if(token.length() < 4){
					continue;
				}
				if(tvshow.getString("title").toLowerCase().contains(token.toLowerCase())){
					flag = 1;
					break;
				}
			}
			
			for(String token: tokens){
				if(token.length() < 4){
					continue;
				}
				if(tvshow.getString("year").toLowerCase().contains(token.toLowerCase())){
					flag = 1;
					break;
				}
			}
			
			for(String token: tokens){
				if(token.length() < 4){
					continue;
				}
				if(tvshow.getString("actors").toLowerCase().contains(token.toLowerCase())){
					flag = 1;
					break;
				}
			}
			
			if(flag == 0){
				IMDBresultIrrelevant.add(tvshow);
				IMDBresult.remove(tvshow);
				i--;
			}
		}
		

		for(int i = 0; i < LEGACYresult.size(); i++){
			flag = 0;
			tvshow = LEGACYresult.get(i);
			for(String token: tokens){
				if(token.length() < 4){
					continue;
				}
				if(tvshow.getString("title").toLowerCase().contains(token.toLowerCase())){
					flag = 1;
					break;
				}
			}
			
			for(String token: tokens){
				if(token.length() < 4){
					continue;
				}
				if(tvshow.get("year").toString().toLowerCase().contains(token.toLowerCase())){
					flag = 1;
					break;
				}
			}
			
			for(String token: tokens){
				if(token.length() < 4){
					continue;
				}
				if(tvshow.get("people").toString().toLowerCase().contains(token.toLowerCase())){
					flag = 1;
					break;
				}
			}
			
			if(flag == 0){
				LEGACYresultIrrelevant.add(tvshow);
				LEGACYresult.remove(tvshow);
				i--;
			}
		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("searchTerms");
		String result = "";

		//do the search here
		TVRAGESearch tv = new TVRAGESearch(name, TVRAGEresult);
		IMDBSearch imdb = new IMDBSearch(name, IMDBresult);
		LEGACYSearch legacy = new LEGACYSearch(name, LEGACYresult);
		
		imdb.start();
		legacy.start();
		tv.start();
		
		try {
			legacy.join(3000);
			imdb.join(3000);
			tv.join(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sortbyRelevance(name);
		
		getFinalForm();
		
		request.setAttribute("results", (Object) finalForm);
		
		request.getRequestDispatcher("/WEB-INF/results.jsp").forward(request, response);
		
		LEGACYresult.clear();
		IMDBresult.clear();
		TVRAGEresult.clear();
		LEGACYresultIrrelevant.clear();
		IMDBresultIrrelevant.clear();
	}
}
