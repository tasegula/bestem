package com.endava.fedes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;;


/*
	Class  that queries the syncwebserver

	It receives the query and a reference to a LinkedList, and returns the result of the query
	as the first element in the list.
*/
public class LEGACYSearch extends Thread {
	
	String query;
	LinkedList<JSONObject> result;
	
	public LEGACYSearch(String query, LinkedList<JSONObject> result) {
		this.query = query;
		this.result = result;
	}
	
	/* The method which converts blank spaces to '%20' */
	public static String modifyQueryString(String x) throws UnsupportedEncodingException {
		return x.replaceAll("\\ ", "%20");
	}
	
	/* The method which interogates the syncwebserver. It returns the server's response 
	as a String. */
	private String igor() {
		HashMap<String, List<String>> headers;
		URL url;
		URLConnection con;
		try {
			url = new URL("http://127.0.0.1:8081/tracktv?query=" + modifyQueryString(query));
			con = url.openConnection();
		
		
			headers = new HashMap<>(con.getHeaderFields());
			
			if (headers.get(null).get(0).equals("HTTP/1.1 200 OK")) {
				InputStream in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				BufferedReader a = new BufferedReader(new InputStreamReader(in));
			    String str = a.readLine();
	
			    return str;
			}
		}
		catch(IOException e) {}
		return "";
	}
	
	public void run() {
        String auxRes = igor();
        result.addAll(ResponseParser.parseLegacyResponse(auxRes));
    }
}
