package com.endava.fedes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*
 * Servlet which listenes for callbacks.
 * */
public class TVRAGEServlet extends HttpServlet{
	/*
	 * When the callback server sends a response, this servlet intercepts it and makes the 
	 * result available for interrogation thread.
	 * */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		InputStreamReader in = new InputStreamReader(request.getInputStream(), "UTF-8");
		BufferedReader br = new BufferedReader(in);
		String res = br.readLine();
		
		TVRAGESearch.putResult(res);
	}
}
