package com.endava.fedes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;


/*
	Class  that queries the search provider with pooling.

	It receives the query and a reference to a LinkedList, and returns the result of the query
	as the first element in the list.
*/
public class IMDBSearch extends Thread{

	String query;
	LinkedList<JSONObject> result;
	
	public IMDBSearch(String query, LinkedList<JSONObject> result) {
		this.query = query;
		this.result = result;
	}
	
	/* The method which retrieves the query result.
	*/
	private String getInfo()
	{
		try {
			return igor("http://localhost:8082/movies/" + LEGACYSearch.modifyQueryString(query), 25);
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}
	
	/* The method that queries the server while it receives a 202 response code,
	until it receives a 200 response code. 
	This is a recursive method, but we avoid any risk of StackOverflow by limiting the number
	of possible recursions to $counter.*/
	private String igor(String link, int counter)
	{
		HashMap<String, List<String>> headers;
		URL url;
		URLConnection con;
		InputStream in;
		BufferedReader rd;
		String result;
		
		if(counter-- <= 0)
			return "";
		
		try
		{
			url = new URL(link);
			con = url.openConnection();

			/* server-friendly sleep */
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			headers = new HashMap<>(con.getHeaderFields());
			
			result = headers.get(null).get(0);
			
			if(result.equals("HTTP/1.1 202 Accepted"))
			{
				in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				rd = new BufferedReader(new InputStreamReader(in));
			    String str = rd.readLine();
			    
			    String[] stringArray = str.split("\"");
			    String linkNew = null;
			    for(int i = 0; i < stringArray.length; i++)
			    
			    	if(stringArray[i].equals("detail"))
			    	{
			    		linkNew = stringArray[i+2];
			    		break;
			    	}
			    
			    /* linkNew should contain the redirect link */
			    if(linkNew == null)
			    	return "";
			    return igor(linkNew, counter);
			}
			
			if(result.equals("HTTP/1.1 200 OK"))
			{
		    	in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				rd = new BufferedReader(new InputStreamReader(in));
			    String str = rd.readLine();
			    
			    return str;
			}
		}
		catch(IOException e){
		}

		return "";
	}
	
	public void run() {
        String auxRes = getInfo();
        try {
        	result.addAll(ResponseParser.parseAsynchronousResponse(auxRes));
        } catch(Exception e) { }
    }
	
}
